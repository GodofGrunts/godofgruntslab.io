Title: Tetris Clone in Godot 3
Date: 2018-07-19 00:21
Category: Gamedev
Tags: gamedev, godot, tetris
Slug: tetris-in-godot
Authors: GodofGrunts
Summary: WIP Tetirs in Godot

I decided to move on to something a little more challenging and try to recreate Tetris in Godot.

Again, all the "art" was created by me in Krita and is MIT license if any unreasonable person wants to use it.

I'm going to try and follow the [Tetris Guideline](http://tetris.wikia.com/wiki/Tetris_Guideline) as much as I can unless something just ends up being really unreasonable for me to waste my time on since it's just a clone.

So far, I've added the ability to spawn shapes using the [bag randomizer](http://tetris.wikia.com/wiki/Random_Generator). It's not quite fully implemented as I haven't got a check for "Snake Sequence", but I'll use what I have for now as testing.

I've also added the ability to rotate the tetrominoes (that's what Tetris shapes are called apparently), but I'll need to watch more gameplay to make sure they're correctly rotating.

Next, I'll add collision and something to detect if 10 block are next to each other so the line can wipe. 

Should be fun!

Check it out at [my gitlab](https://gitlab.com/GodofGrunts/tetris-clone-godot).
