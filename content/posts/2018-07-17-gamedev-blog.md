Title: Gamedev progress
Date: 2018-07-17
Category: Gamedev
Tags: gamedev, gitlab
Slug: gamedev-progress

I've decided to move my stuff to Gitlab from Github since it has been aquired by Microsoft. I'll start fresh and leave the old cruft over there.

First up is my Tic-tac-toe clone which will be part of a seperate blog post.
