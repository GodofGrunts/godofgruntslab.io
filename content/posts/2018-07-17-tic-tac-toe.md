Title: Tic-Tac-Toe
Date: 2018-07-17 11:04
Category: Gamedev
Tags: gamedev, godot, tic-tac-toe
Slug: tic-tac-toe
Authors: GodofGrunts
Summary: Release of Tic-Tac-Toe

This is my first time creating a game from scratch. I made the "art" using Krita which is MIT if, for some reason, you wanted to use it.

The music, font, and sound effect are listed in the own folders under "LICENSE" if you're curious about them.

Features are pretty basic. X goes first, area2d detection determines what square you clicked in, and either the X or O sprite is revealed depending on who's turn it is.

A check is performed after every click to determine if 3 Xs or 3 Os are lined up. If they are, the victory fanfare is played.

If the above check fails, a check is ran to see if all 9 spots are full. If they are, the "tie" music is played.

If both checks fail, the loop is restarted.

Pretty basic, but I'm happy with it. Original code was compiled with Godot 3.0.4 if you want to compile it yourself.

Check it out at [my gitlab](https://gitlab.com/GodofGrunts/tic-tac-toe-godot).
