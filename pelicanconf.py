#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'GodofGrunts'
SITENAME = u'GodofGrunts\' Dev Blog'
SITEURL = 'godofgrunts.gitlab.io'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Gitlab', 'https://gitlab.com/GodofGrunts/'),
         )

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/godofgrunts'),
          )

TWITTER_USERNAME = 'godofgrunts'
DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
